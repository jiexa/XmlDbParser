package com.malyshev.dao.entities;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public class User extends Entity {
  private String login;
  private String password;
  private String lastName;
  private String firstName;
  private String middleName;
  private Calendar birthDate;
  private Boolean isMale;
  private String email;
  private Calendar createDate;
  
  public User( ) {
  }
  
  public User( String login, String password, String lastName, String firstName, Boolean isMale, Calendar createDate ) {
    this.login = login;
    this.password = password;
    this.lastName = lastName;
    this.firstName = firstName;
    this.isMale = isMale;
    this.createDate = createDate;
  }
  
  public String getLogin( ) {
    return login;
  }
  
  public void setLogin( String login ) {
    this.login = login;
  }
  
  public String getPassword( ) {
    return password;
  }
  
  public void setPassword( String password ) {
    this.password = password;
  }
  
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public Calendar getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Calendar birthDate) {
    this.birthDate = birthDate;
  }

  public Boolean getMale() {
    return isMale;
  }

  public void setMale(Boolean male) {
    isMale = male;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Calendar getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Calendar createDate) {
    this.createDate = createDate;
  }
  
  @Override
  public String toString( ) {
    return "User {" +
         "lastName='" + lastName + '\'' +
         ", firstName='" + firstName + '\'' +
         ", middleName='" + middleName + '\'' +
         ", birthDate=" + birthDate +
         ", isMale=" + isMale +
         ", email='" + email + '\'' +
         ", createDate=" + createDate +
         '}';
  }
}