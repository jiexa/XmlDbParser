package com.malyshev.dao.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public class Role extends Entity {
  private User user;
  private Boolean isPupil;
  private Boolean isTeacher;
  private Boolean isAdmin;
  
  public User getUser( ) {
    return user;
  }
  
  public void setUser( User user ) {
    this.user = user;
  }
  
  public Boolean getPupil( ) {
    return isPupil;
  }
  
  public void setPupil( Boolean pupil ) {
    isPupil = pupil;
  }
  
  public Boolean getTeacher( ) {
    return isTeacher;
  }
  
  public void setTeacher( Boolean teacher ) {
    isTeacher = teacher;
  }
  
  public Boolean getAdmin( ) {
    return isAdmin;
  }
  
  public void setAdmin( Boolean admin ) {
    isAdmin = admin;
  }
  
  @Override
  public String toString( ) {
    return "Role{" +
         "user=" + user +
         ", isPupil=" + isPupil +
         ", isTeacher=" + isTeacher +
         ", isAdmin=" + isAdmin +
         '}';
  }
}