package com.malyshev.dao.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public class Topic extends Entity {
      
      private String name;
            
      public String getName( ) {
            return name;
      }
      
      public void setName( String name ) {
            this.name = name;
      }
      
      @Override
      public String toString( ) {
            return "Topic {" +
                 "name='" + name + '\'' +
                 '}';
      }
}
