package com.malyshev.dao.entities;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public abstract class Entity {
  
  private Integer id;

  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
}
