package com.malyshev.dao.entities;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public class Task extends Entity {
      
      private Topic topicId;
      private String textTask;
      private Integer complexity;
      private Calendar publishDate;
      
      public Topic getTopicId( ) {
            return topicId;
      }
      
      public void setTopicId( Topic topicId ) {
            this.topicId = topicId;
      }
      
      public String getTextTask( ) {
            return textTask;
      }
      
      public void setTextTask( String textTask ) {
            this.textTask = textTask;
      }
      
      public Integer getComplexity( ) {
            return complexity;
      }
      
      public void setComplexity( Integer complexity ) {
            this.complexity = complexity;
      }
      
      public Calendar getPublishDate( ) {
            return publishDate;
      }
      
      public void setPublishDate( Calendar publishDate ) {
            this.publishDate = publishDate;
      }
      
      @Override
      public String toString( ) {
            return "Task {" +
                 "topicId=" + topicId +
                 ", textTask='" + textTask + '\'' +
                 ", complexity=" + complexity +
                 ", publishDate=" + publishDate +
                 '}';
      }
}