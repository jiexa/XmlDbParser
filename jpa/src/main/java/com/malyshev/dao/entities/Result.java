package com.malyshev.dao.entities;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public class Result extends Entity {
      
      private User userId;
      private Task taskId;
      private Integer pupilAnswer;
      private Integer mark;
      private Calendar completionDate;
      
      public Result( ) {
      }
      
      public Result( User userId, Task taskId, Calendar completionDate ) {
            this.userId = userId;
            this.taskId = taskId;
            this.completionDate = completionDate;
      }
      
      public User getUserId( ) {
            return userId;
      }
      
      public void setUserId( User userId ) {
            this.userId = userId;
      }
      
      public Task getTaskId( ) {
            return taskId;
      }
      
      public void setTaskId( Task taskId ) {
            this.taskId = taskId;
      }
      
      public Integer getPupilAnswer( ) {
            return pupilAnswer;
      }
      
      public void setPupilAnswer( Integer pupilAnswer ) {
            this.pupilAnswer = pupilAnswer;
      }
      
      public Integer getMark( ) {
            return mark;
      }
      
      public void setMark( Integer mark ) {
            this.mark = mark;
      }
      
      public Calendar getCompletionDate( ) {
            return completionDate;
      }
      
      public void setCompletionDate( Calendar completionDate ) {
            this.completionDate = completionDate;
      }
      
      @Override
      public String toString( ) {
            return "Result {" +
                 "userId=" + userId +
                 ", taskId=" + taskId +
                 ", pupilAnswer='" + pupilAnswer + '\'' +
                 ", mark=" + mark +
                 ", completionDate=" + completionDate +
                 '}';
      }
}