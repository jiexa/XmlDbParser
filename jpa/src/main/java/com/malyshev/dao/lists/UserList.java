package com.malyshev.dao.lists;

import com.malyshev.dao.entities.User;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by A.Malyshev on 19.02.17.
 */
@XmlRootElement( name = "users" )
public class UserList extends EntityList< UserList, User > {
      
      private List< User > users;
      
      public UserList( ) {
            this.users = new ArrayList<>( );
      }

      @Override
      public UserList newInstance( ) {
            return new UserList( );
      }
      
      @Override
      @XmlElement
      public void setList( List< User > list ) {
            users = list;
      }
      
      @Override
      public List< User > getList( ) {
            return users;
      }
}
