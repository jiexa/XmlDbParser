package com.malyshev.dao.lists;

import com.malyshev.dao.entities.Task;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by A.Malyshev on 19.02.17.
 */
@XmlRootElement( name = "tasks" )
public class TaskList extends EntityList<TaskList, Task> {
      
      private List< Task > tasks;
      
      public TaskList( ) {
            this.tasks = new ArrayList<>( );
      }
      
      @Override
      public TaskList newInstance( ) {
            return new TaskList();
      }
      
      @Override
      @XmlElement
      public void setList( List< Task > list ) {
            tasks = list;
      }
      
      @Override
      public List< Task > getList( ) {
            return tasks;
      }
}
