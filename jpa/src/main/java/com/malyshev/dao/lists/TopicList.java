package com.malyshev.dao.lists;

import com.malyshev.dao.entities.Topic;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by A.Malyshev on 19.02.17.
 */
@XmlRootElement( name = "topics" )
public class TopicList extends EntityList<TopicList, Topic> {
      
      private List< Topic > topics;
      
      public TopicList( ) {
            this.topics = new ArrayList<>( );
      }
      
      @Override
      public TopicList newInstance( ) {
            return new TopicList();
      }
      
      @Override
      @XmlElement( name = "topic" )
      public void setList( List< Topic > list ) {
            topics = list;
      }
      
      @Override
      public List< Topic > getList( ) {
            return topics;
      }
}
