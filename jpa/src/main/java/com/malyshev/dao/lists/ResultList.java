package com.malyshev.dao.lists;

import com.malyshev.dao.entities.Result;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by A.Malyshev on 20.02.17.
 */
@XmlRootElement( name = "results" )
public class ResultList extends EntityList<ResultList, Result > {
      
      private List< Result > resultList;
      
      public ResultList( ) {
            this.resultList = new ArrayList<>( );
      }
      
      @Override
      public ResultList newInstance( ) {
            return new ResultList();
      }
      
      @Override
      @XmlElement( name = "result" )
      public void setList( List< Result > list ) {
            resultList = list;
      }
      
      @Override
      public List< Result > getList( ) {
            return resultList;
      }
}
