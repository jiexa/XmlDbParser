package com.malyshev.dao.lists;

import com.malyshev.dao.entities.Role;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by A.Malyshev on 19.02.17.
 */
@XmlRootElement(name = "roles")
public class RoleList extends EntityList<RoleList, Role >{
  
  private List<Role > roles;

  public RoleList() {
    this.roles = new ArrayList<>();
  }
  
  @Override
  public RoleList newInstance( ) {
    return new RoleList();
  }
  
  @Override
  @XmlElement(name = "role")
  public void setList( List< Role > list ) {
    roles = list;
  }
  
  @Override
  public List< Role > getList( ) {
    return roles;
  }
}
