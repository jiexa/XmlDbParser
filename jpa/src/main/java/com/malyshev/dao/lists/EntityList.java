package com.malyshev.dao.lists;

import java.util.List;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public abstract class EntityList<T, K> {
  
  public abstract T newInstance();
  
  @XmlTransient
  public abstract void setList(List<K> list );
  
  public abstract List<K> getList();
}
