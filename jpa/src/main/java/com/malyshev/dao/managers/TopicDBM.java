package com.malyshev.dao.managers;

import com.malyshev.dao.entities.Topic;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public class TopicDBM extends DataBaseManager< Topic, TopicDBM > {
      
      private static final Logger LOGGER = Logger.getLogger( TopicDBM.class );
      
      private static final TopicDBM TOPIC_DBM = new TopicDBM( );
      
      public TopicDBM getInstance( ) throws SQLException {
            getConnection();
            return TOPIC_DBM == null ? new TopicDBM( ) : TOPIC_DBM;
      }
      
      private ResultSet resultSet;
      
      public List< Topic > selectAll( )  {
            List< Topic > list = new ArrayList<>( );
            resultSet = getResultSet( Topic.class );
            try {
                  while ( resultSet.next( ) ) {
                        Topic topic = new Topic( );
                        topic.setId( resultSet.getInt( "id" ) );
                        topic.setName( resultSet.getString( "name" ) );
                        list.add( topic );
                  }
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            return list;
      }
      
      public Topic selectById( Integer id ) throws SQLException {
            String query = "SELECT * FROM education.topic WHERE  id = ?";
            PreparedStatement preparedStatement = DataBaseManager.getConnection( )
                 .prepareStatement( query );
            preparedStatement.setInt( 1, id );
            resultSet = preparedStatement.executeQuery( );
            
            Topic topic = null;
            if ( resultSet.next( ) ) {
                  topic = new Topic( );
                  topic.setId( resultSet.getInt( "id" ) );
                  topic.setName( resultSet.getString( "name" ) );
            }
            return topic;
      }
      
      @Override
      public void insert( Topic entity ) {
            String query =
                 "INSERT INTO " + "education." + entity.getClass( ).getSimpleName( ).toLowerCase( );
            query = query + "(name) VALUES (?);";
            try {
                  PreparedStatement preparedStatement = DataBaseManager.getConnection( )
                       .prepareStatement( query );
            
                  preparedStatement.setString( 1, entity.getName( ) );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
      }
      
      @Override
      public void update( Topic entity ) {
            
      }
}
