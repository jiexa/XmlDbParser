package com.malyshev.dao.managers;

import com.malyshev.dao.entities.Task;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public class TaskDBM extends DataBaseManager<Task, TaskDBM> {
      
      private static final Logger LOGGER = Logger.getLogger( TaskDBM.class );
      
      private static final TaskDBM TASK_DBM = new TaskDBM( );
      
      public TaskDBM getInstance( ) throws SQLException {
            return TASK_DBM == null ? new TaskDBM( ) : TASK_DBM;
      }
      
      private ResultSet resultSet;
      private TopicDBM topicDBM;
      
      public List< Task > selectAll( )  {
            List< Task > list = new ArrayList<>( );
            try {
                  resultSet = getResultSet(Task.class );
                  while ( resultSet.next( ) ) {
                        Task task = new Task( );
                        fillSelectBody(task);
                        list.add( task );
                  }
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
      
            return list;
      }
      
      public Task selectById(Integer id ) throws SQLException {
            String query = "SELECT * FROM education.task WHERE  id = ?" ;
            PreparedStatement preparedStatement = DataBaseManager.getConnection().prepareStatement( query );
            preparedStatement.setInt( 1, id);
            resultSet = preparedStatement.executeQuery();
      
            Task task = null;
            if ( resultSet.next( ) ) {
                  task = new Task( );
                  fillSelectBody(task);
            }
            return task;
      }
      
      
      private void fillSelectBody( Task task ) throws SQLException {
            topicDBM = new TopicDBM();
            task.setId( resultSet.getInt( "id" ) );
            task.setTopicId( topicDBM.selectById( resultSet.getInt( "topic_id" ) ) );
            task.setTextTask( resultSet.getString( "text_task" ) );
            task.setComplexity( resultSet.getInt( "complexity" ) );
            Calendar cal = Calendar.getInstance( );
            cal.setTime( resultSet.getDate( "publish_datetime" ) );
            task.setPublishDate( cal );
      }
      
      @Override
      public void insert( Task entity )  {
            topicDBM = new TopicDBM();
            String query =
                 "INSERT INTO " + "education." + entity.getClass( ).getSimpleName( ).toLowerCase( );
            query = query + "(topic_id, text_task, complexity, publish_datetime) VALUES (?, ?, ?, ?);";
            try {
                  PreparedStatement preparedStatement = DataBaseManager.getConnection( )
                       .prepareStatement( query );
            
                  preparedStatement.setInt( 1, entity.getTopicId().getId());
                  preparedStatement.setString( 2, entity.getTextTask( ) );
                  preparedStatement.setInt( 3, entity.getComplexity( ) );
                  preparedStatement.setDate( 4, new Date(entity.getPublishDate( ).getTimeInMillis()) );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
      }
      
      @Override
      public void update( Task entity ) {
            
      }
}
