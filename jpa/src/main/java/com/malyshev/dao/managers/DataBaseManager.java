package com.malyshev.dao.managers;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

/**
 *
 *  Abstract class for  working with DB
 * Created by A.Malyshev on 19.02.17.
 */
public abstract class DataBaseManager<T, K> {
            
      private static final Logger LOGGER = Logger.getLogger(DataBaseManager.class);
     
      private static Connection conn;
      
      /**
       *  Get connection with DB
       * @return
       * @throws SQLException
       */
      public static Connection getConnection( )  {
            if (conn != null)
                  return conn;
            try {
                  Context initialContext = new InitialContext();
                  Context environmentContext = (Context) initialContext.lookup("java:comp/env");
                  String dataResourceName = "jdbc/PrepSchool";
                  DataSource dataSource = (DataSource ) environmentContext.lookup(dataResourceName);
                  conn = dataSource.getConnection();
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            } catch ( NamingException e ) {
                  LOGGER.error( e );
            }
            return conn;
      }
      
      /**
       * Get  entity working  with certain table  in DB
       * @return instance of  database manager for certain table
       * @throws SQLException
       */
      public abstract K getInstance() throws SQLException;
      
      /**
       * Build query string dynamically setting where-clause
       * @param params map where key is name of column in the DB
       * @return
       */
      public String queryBuilder(Map<String, Object> params, Class<T> clazz) {
            String query = "SELECT * FROM education." + clazz.getSimpleName().toLowerCase() + " WHERE ";
            StringBuilder sb = new StringBuilder( query );
            int i = 1;
            for( String key : params.keySet() ) {
                  sb.append( key + " = " + "?");
                  if (i != params.size()) {
                        sb.append( " AND" );
                        i++;
                  }
            }
            return sb.toString();
      }
      /**
       *  Allow to insert data into <code>table</code>
       * @param table table name in the database (with schema name)
       * @param map params  where <key>name of column </key> in the database
       * <value>desired inserting value</value>
       */
      public void insert( String table, Map< String, Object > map )  {
            String query = "INSERT INTO " + "education." + table;
            query = query + getDynamicPartForInsert( map );
            PreparedStatement preparedStatement = null;
            try {
                  preparedStatement = conn.prepareStatement( query );
      
                  int i = 1;
                  for( String key : map.keySet( ) ) {
                        if ( map.get( key ) instanceof String ) {
                              preparedStatement.setString( i++, ( String ) map.get( key ) );
                        }
                        if ( map.get( key ) instanceof Integer ) {
                              preparedStatement.setInt( i++, ( Integer ) map.get( key ) );
                        }
                        if ( map.get( key ) instanceof Date ) {
                              preparedStatement.setDate( i++, ( Date ) map.get( key ) );
                        }
                        if ( map.get( key ) instanceof Boolean ) {
                              preparedStatement.setBoolean( i++, ( Boolean ) map.get( key ) );
                        }
                  }
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            
      }
      
      /**
       * Supportive method for getting dynamic part of <method>insert</method>
       * @param map params  where <key>name of column </key> in the database
       * <value>desired inserting value</value>
       * @return
       */
      private String getDynamicPartForInsert( Map< String, Object > map ) {
            String str = " (";
            StringBuilder sb = new StringBuilder( );
            sb.append( str );
            
            int k = 1;
            for( String key : map.keySet( ) ) {
                  sb.append( key );
                  if ( map.size( ) != k++ ) {
                        sb.append( ", " );
                  } else {
                        sb.append( ") " );
                  }
            }
            sb.append( "Values (" );
            
            for( int i = 0; i < map.size( ); i++ ) {
                  if ( map.size( ) != i + 1 ) {
                        sb.append( "?, " );
                  } else {
                        sb.append( "?)" );
                  }
            }
            return sb.toString( );
      }
      
      /**
       * Get <code>ResultSet</code> of <code>SELECT</code> query
       * @param clazz
       * @return
       * @throws SQLException
       */
      public ResultSet getResultSet( Class<T> clazz ) {
            getConnection( );
            String table = clazz.getSimpleName().toLowerCase();
            
            String query = "SELECT * FROM education." + table;
            ResultSet resultSet = null;
            try {
                  PreparedStatement preparedStatement = conn.prepareStatement( query );
            
                  resultSet = preparedStatement.executeQuery( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
      
            return resultSet;
      }
      
      /**
       * Delete the record  with <code>id</code> from  the  table corresponding to the  <code>clazz</code> entity
       * @param clazz  POJO that have an entity in the DB
       * @param id of the record
       */
      public void delete(Class<T> clazz, Integer id )  {
            String query = "DELETE FROM education." + clazz.getSimpleName() + " where id = ?";
            PreparedStatement preparedStatement = null;
            try {
                  preparedStatement = conn.prepareStatement( query );
                  preparedStatement.setInt( 1, id );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error(e);
            }
            LOGGER.info( "Record with id = " + id + " was deleted from the table " +  clazz.getSimpleName());
      }
      
      /**
       * Delete all records from  the  table corresponding to the  <code>clazz</code> entity
       * @param clazz  POJO that have an entity in the DB
       */
      public void deleteAll( Class<T> clazz )  {
            String query = "DELETE FROM education." + clazz.getSimpleName();
            try {
                  PreparedStatement preparedStatement = conn.prepareStatement( query );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
      
            LOGGER.info( "Таблица " +  clazz.getSimpleName() + " очищина");
      }
      
      /**
       * Get all records from  a table of DB
       * @return List of <code>T</code> Java entities
       */
      public abstract List<T> selectAll(  );
      
      /**
       * Update the record that matches  <code>entity</code>
       * @param entity
       */
      public abstract void update(T entity);
      
      /**
       * Insert record  to a table
       * @param entity
       * @throws SQLException
       */
      public abstract void insert(T entity);
      
      
}
 