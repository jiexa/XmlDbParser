package com.malyshev.dao.managers;

import com.malyshev.dao.entities.Role;
import com.malyshev.dao.entities.User;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public class RoleDBM extends DataBaseManager<Role, RoleDBM> {
      
      private static final Logger LOGGER = Logger.getLogger( RoleDBM.class );
      
      private static final RoleDBM ROLE_DBM = new RoleDBM( );
      
      public RoleDBM getInstance( ) throws SQLException {
            return ROLE_DBM == null ? new RoleDBM( ) : ROLE_DBM;
      }
      
      private ResultSet resultSet;
      private UserDBM userDBM;
      
      public List< Role > selectAll( )  {
            List< Role > list = new ArrayList<>( );
            resultSet = getResultSet( Role.class );
      
            try {
                  while ( resultSet.next( ) ) {
                        Role role = new Role( );
                        fillSelectBody(role);
                        list.add( role );
                  }
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            return list;
      }
      
      public Role selectById(Integer id ) throws SQLException {
            String query = "SELECT * FROM education.role WHERE  id = ?" ;
            PreparedStatement preparedStatement = DataBaseManager.getConnection().prepareStatement( query );
            preparedStatement.setInt( 1, id);
            resultSet = preparedStatement.executeQuery();
      
            Role role = null;
            if ( resultSet.next( ) ) {
                  role = new Role( );
                  fillSelectBody(role);
            }
            return role;
      }
      
      public Role selectByUserId(Integer id )  {
            String query = "SELECT * FROM education.role WHERE  user_id = ?" ;
            Role role = null;
            try {
                  PreparedStatement preparedStatement = DataBaseManager.getConnection().prepareStatement( query );
                  preparedStatement.setInt( 1, id);
                  resultSet = preparedStatement.executeQuery();
            
                  role = null;
                  if ( resultSet.next( ) ) {
                        role = new Role( );
                        fillSelectBody(role);
                  }
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            return role;
      }
      
      
      public List<Role > selectByParams( Map<String, Object> params )  {
            String query = queryBuilder( params, Role.class );
            List<Role> list = null;
            try {
                  PreparedStatement preparedStatement = DataBaseManager.getConnection( )
                       .prepareStatement( query );
                  int j = 1;
                  for (String key : params.keySet()) {
                        if (params.get( key) instanceof  Integer)
                              preparedStatement.setInt( j++, (Integer ) params.get( key) );
                        if (params.get( key) instanceof  String)
                              preparedStatement.setString( j++, (String ) params.get( key) );
                        if (params.get( key) instanceof Date )
                              preparedStatement.setDate( j++, (Date ) params.get( key) );
                        if (params.get( key) instanceof Boolean )
                              preparedStatement.setBoolean( j++, (Boolean ) params.get( key) );
                  }
                  resultSet = preparedStatement.executeQuery( );
            
                  list = new ArrayList<>(  );
                  while ( resultSet.next( ) ) {
                        Role role = new Role( );
                        fillSelectBody( role );
                        list.add( role );
                  }
            } catch ( SQLException e ) {
                  LOGGER.equals( e );
            }
            return list;
      }
      
     
      
      
      private void fillSelectBody( Role role ) throws SQLException {
            userDBM = new UserDBM();
            role.setId( resultSet.getInt( "id" ) );
            role.setUser( userDBM.selectById(resultSet.getInt( "user_id" ) ));
            role.setPupil( resultSet.getBoolean( "is_pupil" ) );
            role.setTeacher( resultSet.getBoolean( "is_teacher" ) );
            role.setAdmin( resultSet.getBoolean( "is_admin" ) );
      }
      
      @Override
      public void insert( Role entity )  {
            userDBM = new UserDBM();
            String query =
                 "INSERT INTO " + "education." + entity.getClass( ).getSimpleName( ).toLowerCase( );
            query = query + "(user_id, is_pupil, is_teacher, is_admin) VALUES (?, ?, ?, ?);";
            try {
                  PreparedStatement preparedStatement = DataBaseManager.getConnection( )
                       .prepareStatement( query );
            
                  preparedStatement.setInt( 1, entity.getUser( ).getId());
                  preparedStatement.setBoolean( 2, entity.getPupil( ) );
                  preparedStatement.setBoolean( 3, entity.getTeacher( ) == null ? false : entity.getTeacher( ) );
                  preparedStatement.setBoolean( 4, entity.getAdmin( )  == null ? false : entity.getAdmin( ) );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  
                  LOGGER.error( e );
            }
      }
      
      @Override
      public void update( Role entity ) {
            
      }
}
