package com.malyshev.dao.managers;

import com.malyshev.dao.entities.Result;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public class ResultDBM extends DataBaseManager< Result, ResultDBM > {
      
      private static final Logger LOGGER = Logger.getLogger( ResultDBM.class );
      
      private static final ResultDBM RESULTS_DBM = new ResultDBM( );
      
      public ResultDBM getInstance( ) throws SQLException {
            return RESULTS_DBM == null ? new ResultDBM( ) : RESULTS_DBM;
      }
      
      private ResultSet resultSet;
      private UserDBM userDBM;
      private TaskDBM taskDBM;
      
      public List< Result > selectAll( ) {
            List< Result > list = new ArrayList<>( );
            resultSet = getResultSet( Result.class );
      
            try {
                  while ( resultSet.next( ) ) {
                        Result result = new Result( );
                        fillSelectBody( result );
                        list.add( result );
                  }
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            return list;
      }
      
      public Result selectById(Integer id) throws SQLException {
            String query = "SELECT * FROM education.result WHERE  id = ?" ;
            PreparedStatement preparedStatement = DataBaseManager.getConnection().prepareStatement( query );
            preparedStatement.setInt( 1, id);
            resultSet = preparedStatement.executeQuery();
            
            Result result = null;
            if ( resultSet.next( ) ) {
                  result = new Result( );
                  fillSelectBody( result );
            }
            return result;
      }
      
      private void fillSelectBody( Result result ) throws SQLException {
            userDBM = new UserDBM();
            taskDBM = new TaskDBM();
            result.setId( resultSet.getInt( "id" ) );
            result.setUserId( userDBM.selectById( resultSet.getInt( "user_id" ) ) );
            result.setTaskId( taskDBM.selectById( resultSet.getInt( "task_id" ) ) );
            result.setPupilAnswer( resultSet.getInt( "pupil_answer" ) );
            result.setMark( resultSet.getInt( "mark" ) );
            Calendar cal = Calendar.getInstance( );
            cal.setTime( resultSet.getDate( "completion_date" ) );
            result.setCompletionDate( cal );
      }
      
      @Override
      public void insert( Result entity )  {
            String query =
                 "INSERT INTO " + "education." + entity.getClass( ).getSimpleName( ).toLowerCase( );
            query = query + "(user_id, task_id, pupil_answer, completion_date) VALUES (?, ?, ?, ?);";
            try {
                  PreparedStatement preparedStatement = DataBaseManager.getConnection( )
                       .prepareStatement( query );
            
                  preparedStatement.setInt( 1, entity.getUserId( ).getId() );
                  preparedStatement.setInt( 2, entity.getTaskId( ).getId() );
                  preparedStatement.setInt( 3, entity.getPupilAnswer( ) );
                  preparedStatement.setDate( 4, new Date( entity.getCompletionDate( ).getTimeInMillis()) );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
      }
      
      @Override
      public void update( Result entity ) {
            
      }
}
