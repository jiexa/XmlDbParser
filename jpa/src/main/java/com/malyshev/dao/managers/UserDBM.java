package com.malyshev.dao.managers;

import com.malyshev.dao.entities.User;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 19.02.17.
 */
public class UserDBM extends DataBaseManager< User, UserDBM > {
      
      private static final Logger LOGGER = Logger.getLogger( UserDBM.class );
      
      private static final UserDBM USER_DBM = new UserDBM( );
      
      public UserDBM getInstance( ) throws SQLException {
            return USER_DBM == null ? new UserDBM( ) : USER_DBM;
      }
      
      private ResultSet resultSet;
      
      public List< User > selectAll( ) {
            List< User > list = new ArrayList<>( );
            resultSet = getResultSet( User.class );
            
            try {
                  while ( resultSet.next( ) ) {
                        User user = new User( );
                        fillSelectBody( user );
                        list.add( user );
                  }
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            return list;
      }
      
      public List<User> selectByParams( Map<String, Object> params )  {
            String query = queryBuilder( params, User.class );
            List<User> list = null;
            try {
                  PreparedStatement preparedStatement = DataBaseManager.getConnection( )
                       .prepareStatement( query );
                  int j = 1;
                  for (String key : params.keySet()) {
                        if (params.get( key) instanceof Boolean )
                              preparedStatement.setBoolean( j++, (Boolean ) params.get( key) );
                        if (params.get( key) instanceof  Integer)
                              preparedStatement.setInt( j++, (Integer ) params.get( key) );
                        if (params.get( key) instanceof  String)
                              preparedStatement.setString( j++, (String ) params.get( key) );
                        if (params.get( key) instanceof  Date)
                              preparedStatement.setDate( j++, (Date ) params.get( key) );
                  }
                  resultSet = preparedStatement.executeQuery( );
            
                  list = new ArrayList<>(  );
                  while ( resultSet.next( ) ) {
                        User user = new User( );
                        fillSelectBody( user );
                        list.add( user );
                  }
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            return list;
      }
      
      public User selectById( Integer id ) throws SQLException {
            String query = "SELECT * FROM education.user WHERE  id = ?";
            PreparedStatement preparedStatement = DataBaseManager.getConnection( )
                 .prepareStatement( query );
            preparedStatement.setInt( 1, id );
            resultSet = preparedStatement.executeQuery( );
            
            User user = new User( );
            fillSelectBody( user );
            return user;
      }
      
      private void fillSelectBody( User user ) throws SQLException {
            while ( resultSet.next( ) ) {
                  user.setId( resultSet.getInt( "id" ) );
                  user.setLogin( resultSet.getString( "login" ) );
                  user.setPassword( resultSet.getString( "password" ) );
                  user.setLastName( resultSet.getString( "lastname" ) );
                  user.setFirstName( resultSet.getString( "firstname" ) );
                  user.setMiddleName( resultSet.getString( "middlename" ) );
                  Calendar cal = Calendar.getInstance( );
                  if ( resultSet.getDate( "birth_date" ) != null ) {
                        cal.setTime( resultSet.getDate( "birth_date" ) );
                        user.setBirthDate( cal );
                  } else {
                        user.setBirthDate( null );
                  }
                  user.setMale( resultSet.getBoolean( "is_male" ) );
                  cal.setTime( resultSet.getDate( "create_date" ) );
                  user.setCreateDate( cal );
                  user.setEmail( resultSet.getString( "email" ) );
            }
      }
      
      public User selectUserByLoginAndPassword( String login, String password ) {
            String query =
                 "SELECT * FROM education.user WHERE  login = \'" + login + "\' and password = \'"
                      + password + "\'";
            PreparedStatement preparedStatement = null;
            try {
                  preparedStatement = DataBaseManager.getConnection( ).prepareStatement( query );
                  resultSet = preparedStatement.executeQuery( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            
            User user = new User( );
            try {
                  fillSelectBody( user );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            
            return user;
      }
      
      public User selectUserByLogin( String login ) {
            String query =
                 "SELECT * FROM education.user WHERE  login = \'" + login + "\' ";
            PreparedStatement preparedStatement = null;
            try {
                  preparedStatement = DataBaseManager.getConnection( ).prepareStatement( query );
                  resultSet = preparedStatement.executeQuery( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            
            User user = new User( );
            try {
                  fillSelectBody( user );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            
            return user;
      }
      
      @Override
      public void insert( User entity ) {
            String query =
                 "INSERT INTO " + "education." + entity.getClass( ).getSimpleName( ).toLowerCase( );
            query = query
                 + "(lastname, firstname, middlename, create_date, birth_date, is_male, email, login, password ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?); COMMIT ;";
            try {
                  PreparedStatement preparedStatement = DataBaseManager.getConnection( )
                       .prepareStatement( query );
                  
                  preparedStatement.setString( 1, entity.getLastName( ) );
                  preparedStatement.setString( 2, entity.getFirstName( ) );
                  if ( entity.getMiddleName( ) != null ) {
                        preparedStatement.setString( 3, entity.getMiddleName( ) );
                  } else {
                        preparedStatement.setString( 3, null );
                  }
                  preparedStatement
                       .setDate( 4, new Date( entity.getCreateDate( ).getTimeInMillis( ) ) );
                  if ( entity.getBirthDate( ) != null ) {
                        preparedStatement
                             .setDate( 5, new Date( entity.getBirthDate( ).getTimeInMillis( ) ) );
                  } else {
                        preparedStatement.setDate( 5, null );
                  }
                  preparedStatement.setBoolean( 6, entity.getMale( ) );
                  if ( entity.getEmail( ) != null ) {
                        preparedStatement.setString( 7, entity.getEmail( ) );
                  } else {
                        preparedStatement.setString( 7, null );
                  }
                  preparedStatement.setString( 8, entity.getLogin( ) );
                  preparedStatement.setString( 9, entity.getPassword( ) );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
      }
      
      /*TODO: chane it!  boolean -> void */
      public boolean registerUser( User user ) {
            insert( user );
            return true;
      }
      
      public void update( User user ) {
            
            String query = "UPDATE education." + user.getClass( ).getSimpleName( ).toLowerCase( )
                 + " SET lastname=?, firstname=?, middlename=?, birth_date=?, email=? WHERE id = "
                 + user.getId( );
            PreparedStatement preparedStatement = null;
            try {
                  preparedStatement = getConnection( ).prepareStatement( query );
                  preparedStatement.setString( 1, user.getLastName( ) );
                  preparedStatement.setString( 2, user.getFirstName( ) );
                  preparedStatement.setString( 3, user.getMiddleName( ) );
                  if ( user.getBirthDate( ) != null ) {
                        preparedStatement
                             .setDate( 4, new Date( user.getBirthDate( ).getTimeInMillis( ) ) );
                  } else {
                        preparedStatement.setDate( 4, null );
                  }
                  preparedStatement.setString( 5, user.getEmail( ) );
                  preparedStatement.executeUpdate( );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
            }
            LOGGER.info(
                 "User with id = " + user.getId( ) + " was updated" );
            
      }
}
