package com.malyshev;

import com.malyshev.service.xml.Processor;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Main {
      
      private static final Logger LOGGER = Logger.getLogger(Main.class);
      static {
            PropertyConfigurator.configure("jpa/src/main/resources/log4j.properties");
      }
      
      public static void main( String[] args ) throws SQLException {
                  
            Processor processor = new Processor();
//            processor.loadFromDB();
            processor.uploadToDB();
      }
}
