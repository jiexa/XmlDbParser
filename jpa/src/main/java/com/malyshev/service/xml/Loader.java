package com.malyshev.service.xml;

import com.malyshev.dao.entities.Entity;
import com.malyshev.dao.lists.EntityList;
import com.malyshev.dao.managers.DataBaseManager;
import java.io.File;
import java.sql.SQLException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.apache.log4j.Logger;

/**
 * Load data from DB to xml-file
 * Created by A.Malyshev on 19.02.17.
 */
public class Loader<T extends Entity,  L extends EntityList, K extends DataBaseManager > implements
     Runnable {
      
      private static final Logger LOGGER = Logger.getLogger(Loader.class);
      
      private final K dbm;
      private final L entityList;
      private L localList;
      
      public Loader(Class< L > listClazz, Class< K > dbmClazz )
           throws IllegalAccessException, InstantiationException {
            entityList = listClazz.newInstance( );
            dbm = dbmClazz.newInstance();
      }
      
      @Override
      public void run( ) {
            
            try {
                  K localDBM = ( K ) dbm.getInstance( );
                  
                  localList = ( L ) entityList.newInstance( );
                  
                  localList.setList( localDBM.selectAll( ) );
                  
                  marshal( );
                  LOGGER.info( "\nТалица сущности " + entityList.getClass().getSimpleName() + " успешно бейкапирована\n");
                  
            } catch ( JAXBException e ) {
                  LOGGER.error( "Ошибка записи данных в xml. Проверьте " + entityList.getClass().getSimpleName(), e );
            } catch ( SQLException e ) {
                  LOGGER.error( "Ошибка БД при попытке извлечь данные из таблицы, относящейся к сущности " + entityList.getClass().getSimpleName(), e  );
            }
      }
      
      /**
       *  Form xml-file based on Java entity
       * @throws JAXBException
       */
      public void marshal( ) throws JAXBException {
            String pathName = "exportedXml/" + localList.getClass( ).getSimpleName( ) + ".xml";
            File file = new File( pathName );
            JAXBContext jaxbContext = JAXBContext.newInstance( localList.getClass( ) );
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller( );
            
            jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
            
            jaxbMarshaller.marshal( localList, file );
      }
}
