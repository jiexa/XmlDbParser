package com.malyshev.service.xml;

import com.malyshev.dao.entities.Entity;
import com.malyshev.dao.entities.Result;
import com.malyshev.dao.entities.Role;
import com.malyshev.dao.entities.Task;
import com.malyshev.dao.lists.EntityList;
import com.malyshev.dao.lists.ResultList;
import com.malyshev.dao.lists.RoleList;
import com.malyshev.dao.lists.TaskList;
import com.malyshev.dao.lists.TopicList;
import com.malyshev.dao.lists.UserList;
import com.malyshev.dao.managers.DataBaseManager;
import java.io.File;
import java.sql.SQLException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.log4j.Logger;

/**
 * Upload data from xml-file to database tables
 * Created by A.Malyshev on 21.02.17.
 */
public class Uploader< L extends EntityList, K extends DataBaseManager, T extends Entity > implements
     Runnable {
      
      private static final Logger LOGGER = Logger.getLogger( Uploader.class );
      
      private final K dbm;
      private L entityList;
      
      public Uploader( Class< L > listClazz, Class< K > dbmClazz )
           throws IllegalAccessException, InstantiationException {
            this.dbm = dbmClazz.newInstance( );
            this.entityList = listClazz.newInstance( );
      }
      
      
      @Override
      public void run( ) {
            try {
                  unmarshal( );
                  for( Object obj : entityList.getList( ) ) {
                        synchronized (Processor.lock) {
                              T entity = ( T ) obj;
                              checkAndWait( entity );
                              dbm.insert( entity );
                              addInsertedEntity( entity );
                        }
                  }
                  LOGGER.info( "\nInsert сущности " + entityList.getClass( ).getSimpleName( )
                       + " выполнен успешно\n" );
//                  }
                  
            } catch ( JAXBException e ) {
                  LOGGER.error(
                       "Ошибка чтения из xml. Проверьте " + entityList.getClass( ).getSimpleName( )
                            + ".xml", e );
            } catch ( InterruptedException e ) {
                  LOGGER.error(e);
            }
      }
      
      /**
       * Get Java entity with  filled  data from xml-file
       * @throws JAXBException
       */
      public void unmarshal( ) throws JAXBException {
            File file = new File(
                 "exportedXml/" + entityList.getClass( ).getSimpleName( ) + ".xml" );
            JAXBContext jaxbContext = JAXBContext.newInstance( entityList.getClass( ) );
            
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller( );
            entityList = ( L ) unmarshaller.unmarshal( file );
      }
      
      /**
       * Check if there are all required dependant records and wait if not
       * @param entity
       * @throws InterruptedException
       */
      private void checkAndWait( T entity ) throws InterruptedException {
            if ( entityList instanceof TaskList ) {
                  Task task = ( Task ) entity;
                  while ( !Processor.topicMap.containsKey( task.getTopicId( ).getId( ) ) ) {
                        Processor.lock.wait();
                  }
            }
            if ( entityList instanceof RoleList ) {
                  Role role = ( Role ) entity;
                  while ( !Processor.userMap.containsKey( role.getUser( ).getId( ) ) ) {
                        Processor.lock.wait();
                  }
            }
            if ( entityList instanceof ResultList ) {
                  Result result = ( Result ) entity;
                  while ( !Processor.userMap.containsKey( result.getUserId( ).getId( ) )
                       || !Processor.taskMap.containsKey( result.getTaskId( ).getId( ) ) ) {
                        Processor.lock.wait();
                  }
            }
      }
      
      /**
       * Add  <code>id</code> of inserted data into DB to the collection
       * @param entity
       */
      private void addInsertedEntity( T entity ) {
            if ( entityList instanceof TopicList ) {
                  Processor.topicMap.put( entity.getId( ), entity.getId( ) );
                  Processor.lock.notifyAll();
            }
            if ( entityList instanceof UserList ) {
                  Processor.userMap.put( entity.getId( ), entity.getId( ) );
                  Processor.lock.notifyAll();
            }
            if ( entityList instanceof TaskList ) {
                  Processor.taskMap.put( entity.getId( ), entity.getId( ) );
                  Processor.lock.notifyAll();
            }
      }
}
