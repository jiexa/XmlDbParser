package com.malyshev.service.xml;

import com.malyshev.dao.lists.EntityList;
import com.malyshev.dao.lists.ResultList;
import com.malyshev.dao.lists.RoleList;
import com.malyshev.dao.lists.TaskList;
import com.malyshev.dao.lists.TopicList;
import com.malyshev.dao.lists.UserList;
import com.malyshev.dao.managers.DataBaseManager;
import com.malyshev.dao.managers.ResultDBM;
import com.malyshev.dao.managers.RoleDBM;
import com.malyshev.dao.managers.TaskDBM;
import com.malyshev.dao.managers.TopicDBM;
import com.malyshev.dao.managers.UserDBM;
import com.malyshev.service.xml.Loader;
import com.malyshev.service.xml.Uploader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by A.Malyshev on 22.02.17.
 */
public class Processor {
      public static final Object lock = new Object();
      public static ConcurrentHashMap<Integer, Integer> topicMap = new ConcurrentHashMap<>(  );
      public static ConcurrentHashMap<Integer, Integer> userMap = new ConcurrentHashMap<>(  );
      public static ConcurrentHashMap<Integer, Integer> taskMap = new ConcurrentHashMap<>(  );
      
      public void  uploadToDB() {
            Map<Class<? extends EntityList >, Class<? extends DataBaseManager >> serialMap =  new HashMap<>(8);
            serialMap.put( ResultList.class, ResultDBM.class );
            serialMap.put( RoleList.class, RoleDBM.class );
            serialMap.put( TaskList.class, TaskDBM.class );
            serialMap.put( TopicList.class, TopicDBM.class );
            serialMap.put( UserList.class, UserDBM.class );
            
            Thread thread = null;
            try {
                  for (Class key : serialMap.keySet()) {
                        thread = new Thread( new Uploader( key, serialMap.get( key ) ) );
                        thread.start();
                  }
                  
            } catch ( IllegalAccessException e ) {
                  e.printStackTrace( );
            } catch ( InstantiationException e ) {
                  e.printStackTrace( );
            }
      }
      
      public void loadFromDB() {
            Map< Class< ? extends EntityList >, Class< ? extends DataBaseManager > > serialMap = new HashMap<>(
                 8 );
            serialMap.put( ResultList.class, ResultDBM.class );
            serialMap.put( RoleList.class, RoleDBM.class );
            serialMap.put( TaskList.class, TaskDBM.class );
            serialMap.put( TopicList.class, TopicDBM.class );
            serialMap.put( UserList.class, UserDBM.class );
            
            Thread thread = null;
            try {
                  for( Class key : serialMap.keySet( ) ) {
                        thread = new Thread( new Loader( key, serialMap.get( key ) ) );
                        thread.start( );
                        thread.join( );
                  }
                  
            } catch ( IllegalAccessException e ) {
                  e.printStackTrace( );
            } catch ( InstantiationException e ) {
                  e.printStackTrace( );
            } catch ( InterruptedException e ) {
                  e.printStackTrace( );
            }
      }
}
