<%@ page contentType="text/html;charset=UTF-8" language="java"  session="false" %>
<html>
<head>
    <title>Solving taks</title>
</head>
<body>
<h2>Solve it</h2>
<h3>Topic: ${task.topicId.name}</h3>
<h4>Task: ${task.textTask}</h4>

<form action="solving" method="post">
    <table>
        <tr>
            <td><label>Your answer</label></td>
            <td><input type="number" name="answer" placeholder="type just number"/></td>
        </tr>
        <tr>
            <td/>
            <td align="right"><input type="submit" value="Submit"></td>
        </tr>
    </table>
</form>
</body>
</html>
