<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Editing user</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/javaScript.js"></script>
</head>
<body>
<h2>here is the form to change you personal info</h2>
<form action="edit_user" method="post">
    <table>
        <tr>
            <th>Field name</th>
            <th>Your values</th>
        </tr>
        <tr>
            <td>Login</td>
            <td><input type="text" name="editLogin" value="${editingUser.login}"  disabled="disabled"/>
        </tr>
        <tr>
            <td>Lastname</td>
            <td><input type="text" name="editLastName" value="${editingUser.lastName}" />
        </tr>
        <tr>
        <tr>
            <td>Firstname</td>
            <td><input type="text" name="editFirstName" value="${editingUser.firstName}" />
        </tr>
        <tr>
        <tr>
            <td>Middlename</td>
            <td><input type="text" name="editMiddleName" value="${editingUser.middleName == null ? '' : editingUser.middleName}" />
        </tr>
        <tr>
            <td>Birth Day</td>
            <td><input type="text" id="datePicker" name="editBirthDay"  value="${editingUser.birthDate}" />
        </tr>
        <tr>
            <td>E-mail</td>
            <td><input type="text" name="editEmail" value="${editingUser.email}"/>
        </tr>
    </table>
    <input type="submit" value="Update">
</form>
</body>
</html>
