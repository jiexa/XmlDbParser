<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<div>
    <br/>
    <form action="login" method="post">
        <table>
            <tr>
                <td><label for="login">Login: </label></td>
                <td><input type="text" name="login" id="login" value=""placeholder="Input your login"></td>
            </tr>
            <tr>
                <td><label for="password">Password:</label></td>
                <td>
                    <input type="password" name="password" id="password" value=""
                           placeholder="Input your password">
                    <input type="submit" value="Submit">
                </td>
            </tr>
            <tr/>
            <tr>
                <td>or <a href="registration">Register</a></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
