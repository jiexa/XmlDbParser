<%@ page contentType="text/html;charset=UTF-8" language="java" session="false"%>
<html>
<head>
    <title>Registration</title>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
    <script>
      $(function () {

        $("#regForm").validate({

          rules: {
            login: "required",
            firstname: "required",
            lastname: "required",
            email: {
              required: true,
              email: true
            },
            password: {
              required: true,
              minlength: 5
            },
            agree: "required"
          },

          messages: {
            login: "Please enter your first name",
            firstname: "Please enter your first name",
            lastname: "Please enter your last name",
            password: {
              required: "Please provide a password",
              minlength: "Your password must be at least 5 characters long"
            },
            email: "Please enter a valid email address",
            agree: "Please accept our policy"
          },

          submitHandler: function (form) {
            form.submit();
          }
        });

      });</script>
</head>
<body>
<h2>Registration</h2>

<p>Fill the form up to register in the system</p>
<form action="registration" method="post" id="regForm">
    <table>
        <tr>
            <td><label for="login">Login: </label></td>
            <td><input type="text" name="login" id="login"
                       placeholder="username..."></td>
        </tr>
        <tr>
            <td><label for="password">Password:</label></td>
            <td><input type="password" name="password" id="password"
                       placeholder="pass...">
        </tr>
        <tr>
            <td><label for="lastname">Lastname: </label></td>
            <td><input type="text" name="lastname" id="lastname"
                       placeholder="your lastname..."></td>
        </tr>
        <tr>
            <td><label for="firstname">Firstname: </label></td>
            <td><input type="text" name="firstname" id="firstname"
                       placeholder="your firstname..."></td>
        </tr>
        <tr>
            <td>Sex</td>
            <td>
                <select name="sex">
                    <option value="true">М</option>
                    <option value="false">Ж</option>
                </select>
            </td>
        </tr>
        <tr>
            <td/>
            <td align="right">
                <input type="submit" value="Submit" onclick=""></td>
        </tr>
    </table>
</form>
</body>
</html>
