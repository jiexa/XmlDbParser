<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Tasks</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<h2>Solve tasks - train your brain!</h2>
<table width="60%" border="1px solid black">
    <tr>
        <th width="30%">Topic</th>
        <th width="25%">Task</th>
        <th width="15%">Complexity</th>
    </tr>
    <c:forEach items="${tasks}" var="t">
        <tr>
            <td>${t.topicId.name}</td>
            <td><a href="tasks/solving?taskId=${t.id}">${t.textTask}</a> </td>
            <td>${t.complexity}</td>
        </tr>
    </c:forEach>
</table>

<a href="/isods/logout">Logout</a>
</body>
</html>
