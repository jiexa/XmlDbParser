<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Manage users</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<h2> Control panel </h2>
<table width="50%" border="1px solid black">
    <tr>
        <th width="20%">Login</th>
        <th width="25%">Reg. Date</th>
        <th width="10%">Is Admin</th>
        <th width="25%">Action</th>
    </tr>
    <c:forEach items="${roles}" var="r">
        <tr>
            <td>${r.user.login}</td>
            <td>${r.user.createDate.getTime()}</td>
            <td><input type="checkbox" disabled="disabled"  ${r.admin ? "checked" : ""}
                       name="cbIsAdmin"/>
            </td>
            <form action="manage_users" method="post">
                <td><input type="submit" id="" value="Delete"><input type="text" name="selectedUser"
                                                                     value="${r.user.login}"></td>
            </form>
        </tr>
    </c:forEach>
</table>
<br/>
<a href="toggle_notifier">Toggle notifier</a>
</body>
</html>
