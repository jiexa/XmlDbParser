package common.notifiers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 24.02.17.
 */
public class Mailer {
      
      private static final Logger LOGGER = Logger.getLogger( Mailer.class );
      
      public static  void sendMail(String email, String subject, String text) {
            String propFilePath = "/home/jiexa/Documents/mail.properties";
            InputStream input = null;
            Properties props = null;
            try {
                  input = new FileInputStream(propFilePath);
                  props = new Properties();
                  props.load(input);
            } catch ( FileNotFoundException e ) {
                  LOGGER.error( e );
            } catch ( IOException e ) {
                  e.printStackTrace( );
            }
      
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");
            
            String username = props.getProperty("username");
            String password = props.getProperty("password");
            
            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                  protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                  }
            });
            
            try {
                  Message message = new MimeMessage(session);
                  message.setFrom(new InternetAddress());
                  message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
                  message.setSubject(subject);
                  message.setText(text);
                  
                  Transport.send(message);
            } catch (MessagingException e) {
                  throw new RuntimeException(e);
            }
      }
}
