package common;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 23.02.17.
 */
public class Utils {
      private static final Logger LOGGER = Logger.getLogger( Utils.class );
      
      public static Date stringToDate( String stringDate ) {
            DateFormat format = new SimpleDateFormat( "yyyyMMdd" );
            java.util.Date parsed = null;
            try {
                  parsed = format.parse( stringDate );
            } catch ( ParseException e ) {
                  LOGGER.error( e );
            }
            Date date = new Date( parsed.getTime( ) );
            return date;
      }
      public static Calendar stringToCalendar( String stringDate ) {
            DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
            java.util.Date date = null;
            try {
                  date = df.parse(stringDate);
            } catch ( ParseException e ) {
                  e.printStackTrace( );
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return cal;
      }
      
}
