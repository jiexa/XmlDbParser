package common.exception;

/**
 * Created by A.Malyshev on 24.02.17.
 */
public class UserDBMException extends Exception {
      
      private String msg;
      
      public UserDBMException(String msg) {
            this.msg = msg;
      }
      
      @Override
      public String getMessage( ) {
            return msg;
      }
}
