package common.exception;

/**
 * Created by A.Malyshev on 24.02.17.
 */
public class TaskDBMException extends Exception {
      
      private String msg;
      
      public TaskDBMException(String msg) {
            this.msg = msg;
      }
      
      @Override
      public String getMessage( ) {
            return msg;
      }
}
