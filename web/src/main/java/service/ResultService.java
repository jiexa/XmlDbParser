package service;

import com.malyshev.dao.entities.Result;
import com.malyshev.dao.managers.ResultDBM;
import common.exception.ResultDBMException;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 23.02.17.
 */
public class ResultService {
      
      private static final Logger LOGGER = Logger.getLogger( ResultService.class );
      
      private static ResultDBM resultDBM = new ResultDBM( );
      
      public static List< Result > getResultList( ) {
            
            return resultDBM.selectAll( );
      }
      
      public static Result getResultById(Integer id) throws ResultDBMException {
            try {
                  return resultDBM.selectById( id );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
                  throw new ResultDBMException(  "Записи с id = " + id + " не существует в БД"  );
            }
      }
      
      public static void saveResults(Result result) {
            resultDBM.insert( result );
      }
      
      public static void deleteResult( Result result ) {
            resultDBM.delete( Result.class, result.getId() );
      }
      
}
