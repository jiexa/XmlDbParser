package service;

import com.malyshev.dao.entities.Role;
import com.malyshev.dao.entities.User;
import com.malyshev.dao.managers.RoleDBM;
import com.malyshev.dao.managers.UserDBM;
import common.exception.UserDBMException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 23.02.17.
 */
public class UserService {
      
      private static final Logger LOGGER = Logger.getLogger( UserService.class );
      
      private static final String SALT = "security";
      
      private static UserDBM userDBM = new UserDBM( );
      private static RoleDBM roleDBM = new RoleDBM( );
      
      static {
            UserDBM.getConnection( );
      }
      
      public static boolean authorize( String login, String password ) throws UserDBMException {
            String hashedPass = getHashedPass( password );
            if ( userDBM.selectUserByLoginAndPassword( login, hashedPass ).getId( ) != null ) {
                  return true;
            } else {
                  return false;
            }
      }
      
      public static List<User> getUserList() {
            return userDBM.selectAll();
      }
      
      /**
       *  Register  user in the System
       * @param user
       * @return succeed or not
       * @throws UserDBMException
       */
      public static boolean registerUser( User user )
           throws UserDBMException {
            LOGGER.info( "Registration user with login \'" + user.getLogin( ) + "\'" );
            
            user.setPassword( getHashedPass( user.getPassword( ) ) );
            if ( isUserExist( user.getLogin() ) ) {
                  throw new UserDBMException( "Пользователь, с таким логином уже существуют (login = \'" + user.getLogin() + "\')" );
            }
            userDBM.insert( user );
            assignPupilRole( user );
                  
            return true;
      }
      
      public static void assignPupilRole( User user ) {
            
            Role role = new Role( );
            role.setPupil( true );
            role.setUser( getUserByLogin( user.getLogin( ) ) );
            
            roleDBM.insert( role );
      }
      
      private static String getHashedPass( String unhashedPass ) {
            String hashedPassword = generateHash( unhashedPass );
            String hashedPassWithSalt = SALT + hashedPassword;
            String doubleHashedPass = generateHash( hashedPassWithSalt );
            return doubleHashedPass;
      }
      
      private static String generateHash( String input ) {
            StringBuilder hash = new StringBuilder( );
            
            try {
                  MessageDigest sha = MessageDigest.getInstance( "SHA-256" );
                  byte[] hashedBytes = sha.digest( input.getBytes( ) );
                  char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                       'a', 'b', 'c', 'd', 'e', 'f'};
                  for( int idx = 0; idx < hashedBytes.length; ++idx ) {
                        byte b = hashedBytes[ idx ];
                        hash.append( digits[ ( b & 0xf0 ) >> 4 ] );
                        hash.append( digits[ b & 0x0f ] );
                  }
            } catch ( NoSuchAlgorithmException e ) {
                  LOGGER.error( e );
            }
            
            return hash.toString( );
      }
      
      public static void updateUser( User user )  {
            LOGGER.info( "Changing user with login \'" + user.getLogin( ) + "\'" );
            
            userDBM.update( user );
      }
      
      public static User getUserByLoginAndPass( String login, String password ) {
            String hushedPass = getHashedPass( password );
            return userDBM.selectUserByLoginAndPassword( login, hushedPass );
      }
      
      public static User getUserByLogin( String login ) {
            return userDBM.selectUserByLogin( login );
      }
      
      public static void deleteUser(User user) {
            userDBM.delete( User.class, user.getId() );
      }
      
      /**
       * check if user with <code>login</code> exist
       * @param login
       * @return
       */
      private static boolean isUserExist(String login) {
            User user = userDBM.selectUserByLogin( login );
            return  login.equals( user.getLogin() );
      }
      
      public static List<Role> getRoleList(Map<String, Object> params) {
            if (params == null || params.isEmpty())
                  return roleDBM.selectAll();
            return roleDBM.selectByParams( params );
      }
      
      public static Role getUserRole(User user) {
            return roleDBM.selectByUserId( user.getId() );
      }
      
      public static List<Role> getRoleList() {
            return roleDBM.selectAll();
      }
      
      public static void deleteRole(Role role) {
            roleDBM.delete( Role.class, role.getId() );
            deleteUser( role.getUser() );
      }
      
}
