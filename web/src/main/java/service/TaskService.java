package service;

import com.malyshev.dao.entities.Task;
import common.exception.TaskDBMException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.malyshev.dao.managers.TaskDBM;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 23.02.17.
 */
public class TaskService {
      
      private static final Logger LOGGER = Logger.getLogger( TaskService.class );
      
      private static TaskDBM taskDBM = new TaskDBM( );
      
      public static List< Task > getTaskList( ) {
            
            return taskDBM.selectAll( );
      }
      
      public static Task getTaskById(Integer id) throws TaskDBMException {
            try {
                  return taskDBM.selectById( id );
            } catch ( SQLException e ) {
                  LOGGER.error( e );
                  throw new TaskDBMException( "Записи с id = " + id + " не существует в БД" );
            }
      }
      
      public static void deleteTask( Task task ) {
            taskDBM.delete( Task.class, task.getId() );
      }
      
      public static void addTask( Task task ) {
            taskDBM.insert( task );
      }
      
      public static Map< String, Integer > getActionsList( ) {
            Map< String, Integer > actions = new HashMap<>( 8 );
            actions.put( "Редактировать", 1 );
            actions.put( "Удалить", 2 );
            
            return actions;
      }
      
}
