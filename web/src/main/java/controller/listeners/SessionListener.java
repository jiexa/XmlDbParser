package controller.listeners;

import com.malyshev.dao.entities.Role;
import com.malyshev.dao.entities.User;
import common.notifiers.Mailer;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import org.apache.log4j.Logger;
import service.UserService;

/**
 * Created by A.Malyshev on 27.02.17.
 */
@WebListener
public class SessionListener implements HttpSessionAttributeListener {
      
      private static final Logger LOGGER = Logger.getLogger( SessionListener.class );
      
      private void mailToAdmin(HttpSessionBindingEvent event) {
            Boolean isNotifierOn = (Boolean) event.getSession().getAttribute( "isNotifierOn" );
            if (isNotifierOn == null) {
                  isNotifierOn = true;
                  event.getSession().setAttribute( "isNotifierOn", isNotifierOn );
            }
            
            if (isNotifierOn) {
                  
                  User user =  (User) event.getSession().getAttribute( "user" );
                  Role role = UserService.getUserRole( user );
      
                  Map<String, Object> params = new HashMap<>(  );
                  params.put( "is_admin",  true);
                  List<Role> recipientList = UserService.getRoleList( params );
                  if ( role.getAdmin()) {
                        for (Role recipient : recipientList) {
                              Mailer.sendMail( recipient.getUser().getEmail().trim(), "Text",
                                   "Admin with login \'" + user.getLogin( ) + "\' logged in" );
                              LOGGER.trace( "A letter was sent to " + recipient.getUser().getEmail() );
                        }
                  }
            
            }
      }
      
      
      @Override
      public void attributeAdded( HttpSessionBindingEvent event ) {
            Thread thread = null;
            if (event.getName().equals( "user" )) {
                  thread = new Thread( new Runnable( ) {
                        @Override
                        public void run( ) {
                              mailToAdmin( event );
                        }
                  } );
                  thread.start( );
            }
      }
      
      @Override
      public void attributeRemoved( HttpSessionBindingEvent event ) {
            
      }
      
      @Override
      public void attributeReplaced( HttpSessionBindingEvent event ) {
            
      }
}
