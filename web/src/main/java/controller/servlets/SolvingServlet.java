package controller.servlets;

import com.malyshev.dao.entities.Result;
import com.malyshev.dao.entities.Task;
import com.malyshev.dao.entities.User;
import common.Utils;
import common.exception.TaskDBMException;
import java.io.IOException;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import service.ResultService;
import service.TaskService;

/**
 * Created by A.Malyshev on 27.02.17.
 */
@WebServlet(name = "SolvingServlet", urlPatterns = "/class/tasks/solving")
public class SolvingServlet extends HttpServlet {
      
      private static final Logger LOGGER = Logger.getLogger( TaskService.class );
      
      private Task task;
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
      
            try {
                  task  = TaskService.getTaskById( Integer.valueOf( req.getParameter( "taskId" ) ));
            } catch ( TaskDBMException e ) {
                  LOGGER.error( e );
            }
            req.setAttribute( "task", task );
            
            req.getRequestDispatcher( "/view/solving.jsp" ).forward( req, resp );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
      
            Result result = new Result(( User) req.getSession().getAttribute( "user" ), task, Calendar.getInstance() );
            result.setPupilAnswer( Integer.valueOf( req.getParameter( "answer" )) );
      
            ResultService.saveResults( result );
            
            resp.sendRedirect( "/isods/class/tasks" );
      }
}
