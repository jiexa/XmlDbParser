package controller.servlets;

import com.malyshev.dao.entities.Task;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import service.TaskService;

/**
 * Created by A.Malyshev on 23.02.17.
 */
@WebServlet(name = "TasksServlet", urlPatterns = "/class/tasks")
public class TasksServlet extends HttpServlet {
      
      private static final Logger LOGGER = Logger.getLogger( TasksServlet.class );
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
            List< Task > tasks = null;
            tasks = TaskService.getTaskList( );
            req.setAttribute( "tasks", tasks );
            req.getSession( ).setAttribute( "taskList", tasks );
            
            req.getRequestDispatcher( "/view/tasks.jsp" ).forward( req, resp );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
            String param = req.getParameter( "action" );
            LOGGER.info( "param is " + param );
            Integer actionId = new Integer( param );
            
            if ( actionId == 2 ) {
                  TaskService.deleteTask( selectTask( req ) );
                  req.getSession( ).removeAttribute( "taskList" );
                  resp.sendRedirect( "/isods/list" );
            } else if ( actionId == 1 ) {
                  req.getSession( ).setAttribute( "selectedTask", selectTask( req ) );
                  resp.sendRedirect( "/isods/edit" );
            }
//            req.getRequestDispatcher( "/include/list" ).forward( req,resp );
      }
      
      private Task selectTask( HttpServletRequest req ) {
            Task stud = null;
            String selectedTask = req.getParameter( "selectedTask" );
            List< Task > tasks = ( List< Task > ) req.getSession( ).getAttribute( "taskList" );
            for( Task student : tasks ) {
                  if ( student.toString( ).equals( selectedTask ) ) {
                        stud = student;
                  }
            }
            return stud;
      }
}
