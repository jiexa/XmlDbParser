package controller.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by A.Malyshev on 26.02.17.
 */
@WebServlet(name = "ClassServlet", urlPatterns = "/class")
public class ClassServlet extends HttpServlet {
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            req.getRequestDispatcher( "/view/class.jsp" ).forward( req, resp );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
      }
}
