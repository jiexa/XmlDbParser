package controller.servlets;

import com.malyshev.dao.entities.User;
import common.Utils;
import common.exception.UserDBMException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import service.UserService;

/**
 * Created by A.Malyshev on 23.02.17.
 */
@WebServlet( name = "EditUserServlet", urlPatterns = "/edit_user" )
public class EditUserServlet extends HttpServlet {
      
      private static final Logger LOGGER = Logger.getLogger( EditUserServlet.class );
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
            req.setAttribute( "editingUser",
                 req.getSession( ).getAttribute( "user" ) );
            
            req.getRequestDispatcher( "/view/edit_user.jsp" ).forward( req, resp );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
            User selectedUser = ( User ) req.getSession( )
                 .getAttribute( "user" );
            User editedUser = new User( selectedUser.getLogin( ), selectedUser.getPassword( ),
                                                                              req.getParameter( "editLastName" ),  req.getParameter( "editFirstName" ),
                                                                              selectedUser.getMale( ) , selectedUser.getCreateDate( )  );
            editedUser.setId( selectedUser.getId( ) );
            editedUser.setMiddleName( req.getParameter( "editMiddleName" ) );
            String str = req.getParameter( "editBirthDay" );
            if ( str != null ) {
                  editedUser.setBirthDate( Utils.stringToCalendar( str ) );
            }
            editedUser.setEmail( req.getParameter( "editEmail" ) );
            
            UserService.updateUser( editedUser );
            req.getSession( ).setAttribute( "user", editedUser );
            
            LOGGER.info( "Input date processed" );
            
            resp.sendRedirect( "/isods/account" );
      }
      
      
}
