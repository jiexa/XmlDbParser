package controller.servlets;

import com.malyshev.dao.entities.Role;
import com.malyshev.dao.entities.User;
import com.malyshev.dao.entities.User;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import service.UserService;

/**
 * Created by A.Malyshev on 23.02.17.
 */
@WebServlet(name = "UsersManagementServlet", urlPatterns = "/account/manage_users")
public class UsersManagementServlet extends HttpServlet {
      
      private static final Logger LOGGER = Logger.getLogger( UsersManagementServlet.class );
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
            List< Role > roles = null;
            roles = UserService.getRoleList( );
            req.setAttribute( "roles", roles );
            req.getSession( ).setAttribute( "roleList", roles );
            
            req.getRequestDispatcher( "/view/manage_users.jsp" ).forward( req, resp );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
                  UserService.deleteRole( selectUser( req ) );
                  req.getSession( ).removeAttribute( "roleList" );
                  resp.sendRedirect( "/isods/account/manage_users" );
            
            }
      
      private Role selectUser( HttpServletRequest req ) {
            Role role = null;
            String loginOfSelectedUser = req.getParameter( "selectedUser" );
            List< Role > roles = ( List< Role > ) req.getSession( ).getAttribute( "roleList" );
            for( Role r : roles ) {
                  if ( r.getUser().getLogin().equals( loginOfSelectedUser ) ) {
                        role = r;
                  }
            }
            return role;
      }
}
