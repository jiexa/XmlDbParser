package controller.servlets;

import com.malyshev.dao.entities.User;
import common.exception.UserDBMException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import service.UserService;

/**
 * Created by A.Malyshev on 23.02.17.
 */
@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
      private static final Logger LOGGER = Logger.getLogger( LoginServlet.class );
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            
            
            req.getRequestDispatcher( "/view/auth/login.jsp" ).forward( req,resp );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            String login = req.getParameter( "login" );
            String password = req.getParameter( "password" );
            
            try {
                  if (UserService.authorize( login, password )) {
                        LOGGER.info( "Authorized" );
                        User user = UserService.getUserByLoginAndPass( login, password );
                        req.getSession().setAttribute( "user", user );
                        resp.sendRedirect( "/isods/class" );
                  } else {
                        LOGGER.info( "Unauthorized" );
                        req.getRequestDispatcher( "/view/auth/login.jsp" ).forward( req, resp );
                  }
            } catch ( UserDBMException e ) {
                  LOGGER.error(e);
                  resp.sendRedirect( "/isods/error" );
            }
      }
}
