package controller.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by A.Malyshev on 28.02.17.
 */
@WebServlet(name = "NotifierServlet", urlPatterns = "/account/toggle_notifier")
public class NotifierServlet extends HttpServlet {
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            boolean isNotifierOn = (Boolean) req.getSession().getAttribute( "isNotifierOn" );
            if (isNotifierOn) {
                  isNotifierOn = false;
                  req.getSession().setAttribute( "isNotifierOn", isNotifierOn );
            } else {
                  isNotifierOn = true;
                  req.getSession().setAttribute( "isNotifierOn", isNotifierOn );
            }
            
            resp.sendRedirect( "/isods/account/manage_users" );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
      }
}
