package controller.servlets;

import com.malyshev.dao.entities.User;
import common.exception.UserDBMException;
import java.io.IOException;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import service.UserService;

/**
 * Created by A.Malyshev on 23.02.17.
 */
@WebServlet(name = "RegistrationServlet", urlPatterns = "/registration")
public class RegistrationServlet extends HttpServlet {
      
      private static final Logger LOGGER = Logger.getLogger( RegistrationServlet.class );
      
      @Override
      protected void doGet( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            LOGGER.info( "doing GET" );
            req.getRequestDispatcher( "/view/auth/registration.jsp" ).forward( req,resp );
      }
      
      @Override
      protected void doPost( HttpServletRequest req, HttpServletResponse resp )
           throws ServletException, IOException {
            User user = new User(req.getParameter( "login" ) , req.getParameter( "password" ),
                                                            req.getParameter( "lastname" ),  req.getParameter( "firstname" ),
                                                            Boolean.valueOf(req.getParameter( "sex" ) ),  Calendar.getInstance()) ;
            try {
                  if ( UserService.registerUser( user )) {
                        LOGGER.info( "Registered" );
                        user = UserService.getUserByLogin( user.getLogin() );
                        req.getSession().setAttribute( "user", user );
                        resp.sendRedirect( "/isods/class" );
                  }
            } catch ( UserDBMException e ) {
                  LOGGER.error( e );
                  LOGGER.info( "Registration error" );
                  req.getRequestDispatcher( "/error.jsp" ).forward( req, resp );
            }
      }
}
