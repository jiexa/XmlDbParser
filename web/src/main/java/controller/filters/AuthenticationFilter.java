package controller.filters;

import com.malyshev.dao.entities.User;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 * Created by A.Malyshev on 25.02.17.
 */
@WebFilter(filterName = "AuthenticationFilter", urlPatterns = "/*")
public class AuthenticationFilter implements Filter {
      private static final Logger LOGGER = Logger.getLogger( AuthenticationFilter.class );
      
      private ServletContext context;
      
      @Override
      public void init( FilterConfig filterConfig ) throws ServletException {
            context = filterConfig.getServletContext();
            LOGGER.info( "AuthenticationFilter is initialized. Contex is " + context );
      }
      
      @Override
      public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain )
           throws IOException, ServletException {
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse resp = (HttpServletResponse ) response;
            
            String uri = req.getRequestURI();
            LOGGER.info( "Requested URI is " + uri );
      
            resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            resp.setHeader("Pragma", "no-cache"); // HTTP 1.0.
            resp.setDateHeader("Expires", 0);
            
            HttpSession session = req.getSession(false);
                        
            if (session == null && !(uri.endsWith( "welcome" )
                                                            || uri.endsWith( "registration" )
                                                            || uri.endsWith( "login" ))) {
                  LOGGER.warn( "Attempt to enter to the web-site without login" );
                  resp.sendRedirect( "/isods/welcome" );
            } else {
                  chain.doFilter( request,response );
            }
            
      }
      
      @Override
      public void destroy( ) {
            
      }
}
